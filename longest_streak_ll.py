# class Node:
#   def __init__(self, val):
#     self.val = val
#     self.next = None

def longest_streak(head):
    if head is None:
        return 0
    curr = head
    largest_streak = 0
    current_streak = 0
    comparer = head.val
    while curr:
        if curr.val == comparer:
            current_streak += 1
            curr = curr.next
        else:
            comparer = curr.val
            largest_streak = max(current_streak, largest_streak)
            current_streak = 0
    return max(current_streak, largest_streak)
