graph = {
    "a": ["b", "c"],
    "b": ["d"],
    "c": ["e"],
    "d": ["f"],
    "e": [],
    "f": []
}


def breadth_first_print(graph, start):
    queue = [start]
    while queue:
        curr = queue.pop(0)

        print(curr)

        for neighbor in graph[curr]:
            queue.append(neighbor)


breadth_first_print(graph, "a")
