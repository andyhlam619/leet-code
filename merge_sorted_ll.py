class Node:
    def __init__(self, val):
        self.val = val
        self.next = None

    def merge_lists(head_1, head_2):
        if head_1.val < head_2.val:
            start = head_1
            comparer = head_2
        else:
            start = head_2
            comparer = head_1
        temp1 = head_1.next
        temp2 = head_2.next
        start.next = comparer
        while temp1 and temp2:
            min_of = min(temp1.val, temp2.val)
            if temp1.val == min_of:
                comparer.next = temp1
                comparer = temp1
                temp1 = temp1.next
            if temp2.val == min_of:
                comparer.next = temp2
                comparer = temp2
                temp2 = temp2.next
            if temp2:
                comparer.next = temp1
            if temp1:
                comparer.next = temp2
            return start
