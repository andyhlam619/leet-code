# NEET CODE SOLUTION


class Solution:
    def singleNumber(self, nums: int) -> int:
        res = 0
        for x in nums:
            res = res ^ x
        return res

# MY SOLUTION


class Solution2:
    def singleNumber(self, nums: int) -> int:
        result = set()
        for num in nums:
            if num not in result:
                result.add(num)
            else:
                result.remove(num)
        return list(result)[0]
