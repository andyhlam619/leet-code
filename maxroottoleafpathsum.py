def max_path_sum(root):
    if root is None:
        return float("-inf")
    # Base case
    if root.right is None and root.left is None:
        return root.val

    # Recursive case
    max_left = max_path_sum(root.left)
    max_right = max_path_sum(root.right)
    return root.val + max(max_left, max_right)
