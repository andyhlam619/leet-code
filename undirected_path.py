def build_graph(edges):
    graph = {}

    for edge in edges:
        a, b = edge

    if a not in graph:
        graph[a] = []
    if b not in graph:
        graph[b] = []

    graph[a].append(b)
    graph[b].append(a)

    return graph


def undirected_path(edges, node_A, node_B):
    graph = build_graph(edges)
    print(graph)
    stack = [node_A]
    visited = set()
    while stack:
        curr = stack.pop()
        print(curr)
        if curr == node_B:
            return True

        visited.add(curr)

        for neighbor in graph[curr]:
            if neighbor not in visited:
                stack.append(neighbor)

    return False
