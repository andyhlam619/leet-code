class Solution:
    def lengthOfLastWord(self, s: str) -> int:
        stripped_s = s.strip()
        i = len(stripped_s) - 1
        string = ""
        while i >= 0:
            if stripped_s[i] != " ":
                string += stripped_s[i]
            else:
                break
            i -= 1
        return len(string)
