class Solution:
    def replaceElements(self, arr: list[int]) -> list[int]:
        i = len(arr) - 1
        max_val = -1
        while i >= 0:
            arr[i] = max(arr[i], max_val)
            max_val = arr[i]
            i -= 1
        arr.append(-1)
        arr.pop(0)
        return arr
