def tree_levels(root):
  if not root:
    return []
  stack = [[root, 0]]
  levels = []
  while len(stack) > 0:
    curr = stack.pop()
    # curr = [node, level]
    try:
      levels[curr[1]].append(curr[0].val)
      # Adding a node value to an EXISTING level in our levels array.
    except IndexError:
      levels.append([curr[0].val])
      # If that level is not there we are adding a new level in our levels array. Hence the except IndexError.

    if curr[0].right:
      stack.append([curr[0].right, curr[1]+1])
    if curr[0].left:
      stack.append([curr[0].left, curr[1]+1])

  return levels
