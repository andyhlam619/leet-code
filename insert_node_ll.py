class Node:
    def __init__(self, val):
        self.val = val
        self.next = None


def insert_node(head, value, index):
    curr = head

    if index == 0:
        new_head = Node(value)
        new_head.next = head
        return new_head

    i = index - 1
    while i > 0:
        curr = curr.next
        i -= 1
    temp = curr.next
    curr.next = Node(value)
    curr = curr.next
    curr.next = temp
    return head
