def intersection(a, b):
    d = set(a)
    list = []
    for x in b:
        if x in d:
            list.append(x)
    return list
