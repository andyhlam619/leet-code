def reverse_list(head):
    curr = head
    next_node = None
    while curr:
        temp = curr.next
        curr.next = next_node
        next_node = curr
        curr = temp
    return next_node
