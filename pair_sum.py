def pair_sum(numbers, target_sum):
    d = {}
    for i, v in enumerate(numbers):
        diff = target_sum - v
        if diff in d:
            return (d[diff], i)
        else:
            d[v] = i
