class Node:
    def __init__(self, val):
        self.val = val
        self.next = None


def create_linked_list(values):
    if len(values) == 0:
        return None
    head = Node(values[0])
    i = 1
    curr = head
    while i < len(values):
        curr.next = Node(values[i])
        curr = curr.next
        i += 1

    curr.next = None
    return head
